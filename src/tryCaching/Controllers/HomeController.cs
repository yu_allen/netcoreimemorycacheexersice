﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace tryCaching.Controllers
{
    public class HomeController : Controller
    {
        private  IMemoryCache _memoryCache;
        public HomeController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public IActionResult Index()
        {
            ViewData["CacheTime"] = _memoryCache.GetOrCreate("CacheTime", x => {
                x.SlidingExpiration = TimeSpan.FromSeconds(60);
                return DateTime.Now;
            });

            return View();
        }
    }
}
